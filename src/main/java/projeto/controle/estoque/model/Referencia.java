package projeto.controle.estoque.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import projeto.controle.estoque.utils.LocalDateDeserializer;
import projeto.controle.estoque.utils.LocalDateSerializer;

@Entity
@Table(name = "referencia")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Referencia {
	private Long id;
	private String descricao;
	private boolean status = false;
	private Long logUser;
	private LocalDate logDate = LocalDate.now();
	
	@Id
	@GeneratedValue(generator = "referencia_seq", strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "referencia_seq", sequenceName = "referencia_id_seq", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public boolean isStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Column(name="log_user")
	public Long getLogUser() {
		return logUser;
	}
	
	public void setLogUser(Long logUser) {
		this.logUser = logUser;
	}
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@Column(name="log_date")
	public LocalDate getLogDate() {
		return logDate;
	}
	
	public void setLogDate(LocalDate logDate) {
		this.logDate = logDate;
	}
	
}
