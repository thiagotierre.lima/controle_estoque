package projeto.controle.estoque.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import projeto.controle.estoque.utils.LocalDateDeserializer;
import projeto.controle.estoque.utils.LocalDateSerializer;

@Entity
@Table(name = "estoque")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Estoque {
	private Long id;
	private Item item;
	private Long quantidade;
	private LocalDate dataEntrada = LocalDate.now();
	private LocalDate dataSaida;
	private boolean status = false;
	private Long logUser;
	private LocalDate logDate = LocalDate.now();
	
	@Id
	@GeneratedValue(generator = "estoque_seq", strategy = GenerationType.AUTO)
	@SequenceGenerator(name = "estoque_seq", sequenceName = "estoque_id_seq", allocationSize = 1)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getQuantidade() {
		return quantidade;
	}
	
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@Column(name="data_de_entrada")
	public LocalDate getDataEntrada() {
		return dataEntrada;
	}
	
	public void setDataEntrada(LocalDate dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@Column(name="data_de_saida")
	public LocalDate getDataSaida() {
		return dataSaida;
	}
	
	public void setDataSaida(LocalDate dataSaida) {
		this.dataSaida = dataSaida;
	}
	
	public boolean isStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	@Column(name="log_user")
	public Long getLogUser() {
		return logUser;
	}
	
	public void setLogUser(Long logUser) {
		this.logUser = logUser;
	}
	
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@Column(name="log_date")
	public LocalDate getLogDate() {
		return logDate;
	}
	
	public void setLogDate(LocalDate logDate) {
		this.logDate = logDate;
	}

	@OneToOne
	@JoinColumn(name="item_id")
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
}
